package xyz.xixuan.serverHome.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import xyz.xixuan.serverHome.Configurator;
import xyz.xixuan.serverHome.ServerHome;

public class CommandHome implements CommandExecutor {
    ServerHome plugin;
    public CommandHome(ServerHome serverHome){
        this.plugin = serverHome;
    }
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(sender instanceof Player){
            // if Player
            //load from config.yml and teleport
            if(Boolean.parseBoolean(plugin.configurator().getString(Configurator.path_service))){
                ((Player) sender).teleportAsync(plugin.configurator().getLocation());
                //sender.sendPlainMessage("plugin home");
            }else{
                ((Player) sender).teleportAsync(plugin.configurator().getLocation());
                //sender.sendPlainMessage("spawn point");
            }
        }else{
            //if console
            plugin.out2("Console cannot use 'ServerHome' command!");
        }
        return true;
    }
}
