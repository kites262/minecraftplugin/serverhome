package xyz.xixuan.serverHome.command;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import xyz.xixuan.serverHome.Configurator;
import xyz.xixuan.serverHome.ServerHome;

public class CommandSetHome implements CommandExecutor {
    ServerHome plugin;
    public CommandSetHome(ServerHome serverHome){
        this.plugin = serverHome;
    }
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(sender instanceof Player){
            Location location = ((Player) sender).getLocation();

            plugin.configurator().set(Configurator.path_service,true);
            plugin.configurator().set(Configurator.path_location_world,location.getWorld().getName());
            plugin.configurator().set(Configurator.path_location_x,location.getX());
            plugin.configurator().set(Configurator.path_location_y,location.getY());
            plugin.configurator().set(Configurator.path_location_z,location.getZ());
            plugin.configurator().set(Configurator.path_location_pitch,location.getPitch());
            plugin.configurator().set(Configurator.path_location_yaw,location.getYaw());
            plugin.saveConfig();
            plugin.reloadConfig();
            sender.sendPlainMessage("Server Home set at:");
            sender.sendPlainMessage(location.toString());
        }else{
            plugin.out2("Console cannot use 'ServerHome' command!");
        }
        return true;
    }
}
