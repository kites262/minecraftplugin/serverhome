package xyz.xixuan.serverHome;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.Configuration;

import java.util.Objects;

public class Configurator {
    public static final String path_service = "service";
    public static final String path_location_world = "location.world";
    public static final String path_location_x = "location.x";
    public static final String path_location_z = "location.y";
    public static final String path_location_y = "location.z";
    public static final String path_location_pitch = "location.pitch";
    public static final String path_location_yaw = "location.yaw";
    ServerHome plugin;
    Configuration config;
    Location location;
    public Configurator(ServerHome serverHome){
        this.plugin = serverHome;
        config = plugin.getConfig();
        loadLocation();
    }

    public String getString(String path){
        return String.valueOf(config.get(path));
    }

    public Double getDouble(String path){
        return Double.parseDouble(getString(path));
    }
    public void set(String path, Object object){
        config.set(path, object);
    }

     public void loadLocation(){
        location = Objects.requireNonNull(
                Bukkit.getWorld("world")).getSpawnLocation();
        //Service state
        if(config.getBoolean(path_service)){
            //Is "world" illegal?
            if(config.get(path_location_world).equals("world")
                    |config.get(path_location_world).equals("world_nether")
                    |config.get(path_location_world).equals("world_the_end")){
                //alright, load "Location"
                location = new Location(
                        Bukkit.getWorld(getString(path_location_world)),
                        getDouble(path_location_x),
                        getDouble(path_location_y),
                        getDouble(path_location_z),
                        Float.parseFloat(getString(path_location_yaw)),
                        Float.parseFloat(getString(path_location_pitch))
                );
            }
        }
     }

     public Location getLocation(){
        loadLocation();
        return location;
     }

}
