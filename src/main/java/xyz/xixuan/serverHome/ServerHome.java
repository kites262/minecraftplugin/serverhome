package xyz.xixuan.serverHome;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.xixuan.serverHome.command.CommandHome;
import xyz.xixuan.serverHome.command.CommandSetHome;

import java.util.Objects;

public final class ServerHome extends JavaPlugin {
    private Configurator ct;
    @Override
    public void onEnable() {
        out("Loading ServerHome plugin...");
        //config.yml
        saveDefaultConfig();

        //config action
        ct = new Configurator(this);
        out("Service state: " + ct.getString(Configurator.path_service));
        out("Home at: ");
        out(ct.getLocation().toString());

        //register command
        Objects.requireNonNull(
                Bukkit.getPluginCommand("/")).setExecutor(new CommandHome(this)
        );
        Objects.requireNonNull(
                Bukkit.getPluginCommand("serverHome_set")).setExecutor(new CommandSetHome(this)
        );
        out("Configurator loaded.");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void out(String msg){
        this.getLogger().info(msg);
    }

    public void out2(String msg){
        this.getLogger().warning(msg);
    }

    public Configurator configurator(){
        return ct;
    }
}
